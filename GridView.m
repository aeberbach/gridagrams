//
//  GridView.m
//  TargetAnagrams
//
//  Created by Adam Eberbach on 20/09/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "GridView.h"

@implementation GridView

@synthesize gridButton1, gridButton2, gridButton3, gridButton4, gridButton5;
@synthesize gridButton6, gridButton7, gridButton8, gridButton9;
@synthesize newButton, solveButton, acceptButton, bsButton;
@synthesize gridImageView;
@synthesize backgroundImageView;
@synthesize gameModel;
@synthesize gridButtonPresses;
@synthesize gridWord, candidateWord, candidateWordString;
@synthesize foundWordsTextView;
@synthesize progressLabel;
@synthesize wordLabel;
@synthesize foundWordsList;
@synthesize solutionCount;
@synthesize foundWordsCount;
@synthesize findProgress;
@synthesize activityIndicator;

// a button-centric view of what goes on in this game
// New button is never disabled
/*************************************************************************************/
// New
// grid buttons - all back to unused (clear gridButtonPresses also)
// BS button - disabled
// Accept button - disabled
// Solve button - enabled
// other - clear candidate word

// start a new game with a new word
- (IBAction)newButton:(id)sender {
	
	char scrambledWord[GRID_COUNT + 1];
	
	// start a new game (forget all found words, pick a new mystery word) store scrambled word in the UI
	gridWord = [gameModel newScrambledWord];
	
	// newScrambledWord contains a check that a secondary thread isn't currently doing something 
	// unstoppable. If it returns nil, can't start a new game right now, let user tap again in 
	// a second.
	if(gridWord == nil)
		return;
	
	// any existing solution is invalid - must wait for KVO callback to enable
	solveButton.enabled = NO;
	[solveButton setTitle:@"" forState:UIControlStateNormal];
	foundWordsCount = 0;

	findProgress.progress = 0.0;
	progressLabel.text = @"Calculating solutions... (go ahead and play)";	
	
	[foundWordsList release];
	foundWordsList = [[NSMutableString alloc] initWithCapacity:GRID_COUNT];
	foundWordsTextView.text = nil;
	
	[gridWord getCString:scrambledWord maxLength:(GRID_COUNT + 1) encoding:NSASCIIStringEncoding];

	// set the scrambled word's letters on the grid, give the centre a red background
	[gridButton1 setImage:[self imageForCharacter:scrambledWord[0]] forState:UIControlStateNormal];
	[gridButton2 setImage:[self imageForCharacter:scrambledWord[1]] forState:UIControlStateNormal];
	[gridButton3 setImage:[self imageForCharacter:scrambledWord[2]] forState:UIControlStateNormal];
	[gridButton4 setImage:[self imageForCharacter:scrambledWord[3]] forState:UIControlStateNormal];
	[gridButton5 setImage:[self imageForCharacter:scrambledWord[4]] forState:UIControlStateNormal];
	[gridButton6 setImage:[self imageForCharacter:scrambledWord[5]] forState:UIControlStateNormal];
	[gridButton7 setImage:[self imageForCharacter:scrambledWord[6]] forState:UIControlStateNormal];
	[gridButton8 setImage:[self imageForCharacter:scrambledWord[7]] forState:UIControlStateNormal];
	[gridButton9 setImage:[self imageForCharacter:scrambledWord[8]] forState:UIControlStateNormal];

	[gridButton5 setBackgroundImage:[UIImage imageNamed:@"red.png"] forState:UIControlStateNormal];

	[self wordEntryStartState];
}	

/*************************************************************************************/
// Solve 
// grid buttons - disabled
// BS button - disabled
// Accept button - disabled
// Solve button - disabled

// solve the problem, display all known words
- (IBAction)solveButton:(id)sender {
	// all grid buttons become disabled
	for(int i = 1; i < GRID_COUNT + 1; i++)
		[self setGridButtonMode:i enabled:NO];
	// backspace, solve and accept are also disabled
	bsButton.enabled = NO;
	[self setAcceptButtonMode:kWordNotAcceptable];
	solveButton.enabled = NO;
	
	NSArray* solutionArray = [gameModel solve]; 
	foundWordsTextView.text = [solutionArray componentsJoinedByString:@" "];
	NSString* labelText = [NSString stringWithFormat:@"%d words. Play again? Tap \"New\".", [solutionArray count]];
	progressLabel.text = labelText;

}

/*************************************************************************************/
// Accept
// grid buttons - all back to unused (clear gridButtonPresses also)
// BS button - disabled
// Accept button - disabled
// Solve button - enabled

- (IBAction)acceptWordButton:(id)sender {
	
	[gameModel wordSubmit:candidateWord];
	foundWordsCount++;
	[self progressComment];
	[self updateFoundWords:candidateWordString];
	[self wordEntryStartState];
}

/*************************************************************************************/
// BS button
// grid buttons - re-enable the last grid button pressed, remove its entry from 
// gridButtonPresses, remove last character from candidate word array
// BS button
// Accept button
// Solve button

- (IBAction)bsButton:(id)sender {

	// save the tag value from and then remove the record of last-pressed keys
	int tag = [[gridButtonPresses lastObject] intValue];
	[gridButtonPresses removeLastObject];
	// re-enable the grid button 
	[self setGridButtonMode:tag enabled:YES];
	// disable the backspace button if that was the last one
	if([gridButtonPresses count] == 0) 
		bsButton.enabled = NO;
	
	// remove the last character from the candidate word and string
	[candidateWord removeLastObject];
	[candidateWordString deleteCharactersInRange:NSMakeRange([candidateWordString length] - 1, 1)];
	
	//candidateWordString = (NSMutableString*)[candidateWordString substringToIndex:[candidateWordString length] - 1];
	wordLabel.text = candidateWordString;
	
	// update the status of the accept button according to the game model
	[self setAcceptButtonMode:[gameModel checkWord:candidateWord]];

}

/*************************************************************************************/
// grid button
// grid buttons - disable button just pressed; add its tag to gridButtonPresses, 
// add character to candidate word array. 
// BS button - enabled
// Accept button - Set state of accept button depending on  gameModel status according 
// to candidate word array.
// Solve button - enabled

// all the 9 buttons are targeted here, differentiated by tag
- (IBAction)gridButtons:(id)sender {
	
	// what's the value of this sender's tag?
	int tag = [sender tag];

	// record the button responsible in case it needs to be re-enabled following backspace
	[gridButtonPresses addObject:[NSNumber numberWithInt:tag]];
	
	// mark the button as in use
	[self setGridButtonMode:tag enabled:NO];
	
	// add the character that this button represents to the candidate word array
	char scrambledWord[GRID_COUNT + 1];
	[gridWord getCString:scrambledWord maxLength:(GRID_COUNT + 1) encoding:NSASCIIStringEncoding];
	[candidateWord addObject:[NSNumber numberWithChar:(*(scrambledWord + tag - 1)) ]];
	
	
	// and display the word so far
	[candidateWordString appendFormat:@"%c", *(scrambledWord + tag - 1)];
	wordLabel.text = candidateWordString;
	
	// update the status of the accept button according to the game model
	[self setAcceptButtonMode:[gameModel checkWord:candidateWord]];
		
	// since there must now be at least one char to remove enable the BS button.
	bsButton.enabled = YES;
}

// a common configuration used when a word is submitted or when a new game is started
- (void)wordEntryStartState {
	// make all grid buttons active
	for(int i = 1; i < GRID_COUNT + 1; i++)
		[self setGridButtonMode:i enabled:YES];
	
	// clear candidate words and recorded grid button presses
	[candidateWord removeAllObjects];
	[candidateWordString release];
	candidateWordString = [[NSMutableString alloc] initWithCapacity:GRID_COUNT];
	
	[gridButtonPresses removeAllObjects];
	
	// there is nothing to backspace
	bsButton.enabled = NO;
	// there is nothing to accept
	[self setAcceptButtonMode:kWordNotAcceptable];
	// clear the text field showing the current word
	wordLabel.text = nil;
}

// set display mode of the accept button to show the validity of the current candidate word
- (void)setAcceptButtonMode:(WordStatus)status {
	
	switch(status) {
		case kWordAcceptable:
			// button should be enabled
			[acceptButton setTitle:@"Accept" forState:UIControlStateNormal];
			acceptButton.enabled = YES;
			break;
		case kWordAlreadyRecorded:
			// button should be disabled
			[acceptButton setTitle:@"\"" forState:UIControlStateNormal];
			acceptButton.enabled = NO;
			break;
		case kWordNotAcceptable:
			// button should be disabled
			[acceptButton setTitle:@"X" forState:UIControlStateNormal];
			acceptButton.enabled = NO;
			break;
	}
}

// set the display mode of a grid button depending on whether it is in use or not in the candidate word
- (void)setGridButtonMode:(int)tag enabled:(Boolean)active {
	
	UIButton* gridButton;
	
	switch(tag) {
		case 1:
			gridButton = gridButton1;
			break;
		case 2:
			gridButton = gridButton2;
			break;
		case 3:
			gridButton = gridButton3;
			break;
		case 4:
			gridButton = gridButton4;
			break;
		case 5:
			gridButton = gridButton5;
			break;
		case 6:
			gridButton = gridButton6;
			break;
		case 7:
			gridButton = gridButton7;
			break;
		case 8:
			gridButton = gridButton8;
			break;
		case 9:
			gridButton = gridButton9;
			break;
	}
	gridButton.enabled = active;
}

// load the correct image for the specified character
- (UIImage*)imageForCharacter:(char)thisChar {
	
	NSMutableString* imageName = [NSMutableString stringWithFormat:@"%c.png", thisChar];
	UIImage* thisImage = [UIImage imageNamed:imageName];
	return thisImage;
}

// update found words display
- (void)updateFoundWords:(NSString*)newWord {

	// add newWord to foundWordsList
	[foundWordsList appendFormat:@"%@ ", newWord];
	
	// display the found words list
	foundWordsTextView.text = foundWordsList;
	
/*	
	NSString *htmlHead = @"<html><head><style>body{background-color:transparent; font: 15pt Helvetica;}</style></head><body>";
	NSString *htmlTail = @"</body></html>";

	NSString *newfoundWordHTML = [[NSString alloc] initWithFormat:@"<FONT COLOR=\"0000FF\">%@ </FONT>", newWord];

	// contains all the found words
	[foundWordsHTML appendFormat:@"%@", newfoundWordHTML];
	
	// build the html string
	NSString *html = [htmlHead stringByAppendingFormat:@"%@ %@", foundWordsHTML, htmlTail];
	
	// and display it all
	[foundWordsWebView loadHTMLString:html baseURL:nil];
*/
}


/*************************************************************************************/
	
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	
	[self.activityIndicator startAnimating];
	
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		// the gameModel object handles the rules of the game - on creation nine-letter words are available
		// but nothing else until KVO says so
		gameModel = [[Model alloc] init];
    }
	gridButtonPresses = [[NSMutableArray alloc] initWithCapacity:GRID_COUNT];
	candidateWord = [[NSMutableArray alloc] initWithCapacity:GRID_COUNT];
	candidateWordString = [[NSMutableString alloc] initWithCapacity:GRID_COUNT];
	foundWordsList = [[NSMutableString alloc] initWithCapacity:GRID_COUNT];

	// make all grid buttons inactive
	for(int i = 1; i < GRID_COUNT + 1; i++)
		[self setGridButtonMode:i enabled:NO];

	// when wordsReady the model can check words (play can start)
	[gameModel addObserver:self forKeyPath:@"wordsReady" options:(NSKeyValueObservingOptionNew) context:NULL];
	// when solutionReady the model can solve the puzzle
	[gameModel addObserver:self forKeyPath:@"solutionReady" options:(NSKeyValueObservingOptionNew) context:NULL];

    return self;
}

- (void)progressComment {
	int first, second, third, fourth;
	float progress;
	
	if(solutionCount) {
		// then the solution is known and the number of words and assessment can be displayed
		progress = (float)foundWordsCount / (float)solutionCount;
		findProgress.progress = progress;
	
		// highest rank comes after finding 3/4 of the words
		first = solutionCount - (solutionCount / 4);
		// then 1/2, 1/4, 1/8
		second = first/2; third = second/2; fourth = third/2;
	
		NSString* assessment;
		
		if(foundWordsCount == solutionCount) {
			assessment = [NSString stringWithFormat:@"PERFECT!"];
		} else if (foundWordsCount > first) {
			assessment = [NSString stringWithFormat:@"Excellent"];
		} else if (foundWordsCount > second) {
			assessment = [NSString stringWithFormat:@"Good"];
		} else if (foundWordsCount > third) {
			assessment = [NSString stringWithFormat:@"OK"];
		} else if (foundWordsCount > fourth) {
			assessment = [NSString stringWithFormat:@"Improving"];
		} else if (foundWordsCount == 0) {
			assessment = [NSString stringWithFormat:@"Can't find even one?"];
		} else {
			assessment = [NSString stringWithFormat:@"Keep trying"];
		}
		progressLabel.text = [NSString stringWithFormat:@"Progress %d/%d: %@", foundWordsCount, solutionCount, assessment];
	} else {
		findProgress.progress = 0.0;
		progressLabel.text = [NSString stringWithFormat:@"Progress %d words", foundWordsCount];
	}
}
/*************************************************************************************/
// Enable features as data sets are loaded

- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqual:@"solutionReady"]) {
		if([change objectForKey:NSKeyValueChangeNewKey])
		{
			solveButton.enabled = YES;
			[solveButton setTitle:@"Solve" forState:UIControlStateNormal];
			solutionCount = [[gameModel solve] count];
			[self progressComment];
		}
		
    }
	if ([keyPath isEqual:@"wordsReady"]) {
		if([change objectForKey:NSKeyValueChangeNewKey])
		{
			[self.activityIndicator stopAnimating];
			newButton.enabled = YES;
			[newButton setTitle:@"New" forState:UIControlStateNormal];
			progressLabel.text = @"Tap \"New\" to play";
		}
    }
	
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	
	UIImage *backgroundImage = [UIImage imageNamed:@"newsprint.png"];
	backgroundImageView.image = backgroundImage;
	
	UIImage *overlayImage = [UIImage imageNamed:@"finerblackgrid.png"];
	gridImageView.image = overlayImage;
	
	wordLabel.text = nil;
	
	findProgress.progress = 0.0;
	newButton.enabled = NO;
	[newButton setTitle:@"" forState:UIControlStateNormal];
	
	progressLabel.text = @"Loading puzzle words, just a second";
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[gridButtonPresses release];
    [super dealloc];
}

@end

