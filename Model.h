//
//  Model.h
//  FindAnagrams
//
//  Created by Adam Eberbach on 26/04/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "constants.h"

#define KEY_CHARACTER				(4)
#define SOWPODS_WORDCOUNT			(267751)	// word count of the original word list
#define SOWPODS9_WORDCOUNT			(40727)		// word count of just 9-letter words
#define SOWPODS_REMAINING_WORDCOUNT	(114575)	// word count of all words less than 9 letters
// with one character fixed in every word (the red square) there are 256 possible answers
#define MAX_FIND_WORDS				(256)

@interface Model : NSObject {

	// semaphores for readiness
	Boolean				wordsReady;
	Boolean				dictionaryReady;
	Boolean				solutionReady;
	
	// This dicitonary uses sorted words as keys and contains array objects containing 1..n unsorted words
	NSDictionary*		solutionDictionary;
	// foundWords is the list of word the player has already found. 
	NSMutableArray*		foundWords;
	// unsortedWords is the rest of the dictionary without the nine letter words
	NSArray*			unsortedWords;
	// the dictionary but with each word sorted into alpha order (HUG becomes GHU)
	NSArray*		sortedWords;
	// to reduce startup speed all 9-letter words are in a smaller separate file
	NSArray*			nineLetterWords;

	// the current word making up the puzzle
	NSMutableString*	currentGridWord;
	// the current scrambled word making up the puzzle
	NSMutableString*	currentScrambledWord;
	
	NSArray*			answerSet;
	
	// the only thing needed from the scrambled word is the character that
	// is the key, for comparison when the UI checks if a word is valid.
	NSNumber*			keyValue;
}

@property Boolean wordsReady;
@property Boolean dictionaryReady;
@property Boolean solutionReady;

@property (nonatomic, retain) NSDictionary* solutionDictionary;
@property (nonatomic, retain) NSArray* answerSet;

@property (nonatomic, retain) NSMutableArray* foundWords;
@property (nonatomic, retain) NSArray* sortedWords;
@property (nonatomic, retain) NSArray* unsortedWords;
@property (nonatomic, retain) NSArray* nineLetterWords;

@property (nonatomic, retain) NSMutableString* currentGridWord;
@property (nonatomic, retain) NSMutableString* currentScrambledWord;
@property (nonatomic, retain) NSNumber*	keyValue;

// start a new game and get the scrambled word
- (NSMutableString*)newScrambledWord;
// submit a found word to the model
- (void)wordSubmit:(NSMutableArray*)wordArray;
// see if a word is an acceptable answer
- (WordStatus)checkWord:(NSMutableArray*)wordArray;
// get all answers
- (NSArray*)solve;


// internal use functions
- (NSArray*)solveWords:(NSString*)inputWord;
- (NSMutableString*)freshGridWord;
- (NSMutableString*)wordFromArray:(NSMutableArray*)wordArray;
- (NSMutableString*)scrambleWord:(NSMutableString*)inputWord;
- (void)findAnswers;

- (void)dealloc;
- (id) init;

@end
