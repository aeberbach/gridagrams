#import "candidateWordView.h"

@implementation candidateWordView

@synthesize currentWord;


- (void)drawRect:(CGRect)rect {

	char buffer[GRID_COUNT+1];
	NSNumber* thisValue;
	char* cursor = buffer;
	
	if([currentWord count]) {
		NSEnumerator* charEnumerator = [currentWord objectEnumerator];
		while(thisValue = [charEnumerator nextObject])
		{
			*cursor = [thisValue charValue];
			cursor++;
		}
	} else {
		buffer[0] = '\0';
	}
		
	cursor = buffer;
	
	float x_offset = 0.0;
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	// load the big image containing all the characters
	UIImage* alphaStrip = [UIImage imageNamed:@"trans_alpha_strip_smaller.tiff"];

	while(*cursor) {

		// get the offsets for the image of the requested character
		alpha_offset charOffset = alpha_map[*cursor];

		// for large trans_alpha image
		CGImageRef imageRef = CGImageCreateWithImageInRect(alphaStrip.CGImage, CGRectMake(charOffset.x, 0.0, charOffset.width, alphaStrip.size.height));
		CGContextDrawImage(context, CGRectMake(x_offset, 0.0, charOffset.width, self.frame.size.height), imageRef);
		CGImageRelease(imageRef);

		x_offset += charOffset.width;
		cursor++;
	}
}

-(void)clearChars {
	[currentWord removeAllObjects];
}

-(void)addChar:(char)newChar {
	[currentWord addObject:[NSNumber numberWithChar:newChar]];
}

-(void)removeChar {
	[currentWord removeLastObject];
}

- (id)init {
	[super init];
	return self;
}

- (id)initWithFrame:(CGRect)frame {
		
	[super initWithFrame:frame];
	
	currentWord = [[NSMutableArray alloc] initWithCapacity:GRID_COUNT+1];
	
	return self;
}

- (void)dealloc {
	[currentWord release];
	[super dealloc];
}

@end
