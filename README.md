# README #

This is the first thing I ever put on the App Store. It generates and solves the 9-letter "Target" puzzle you find in some newspapers. I made about $100 in the early days with this.

### What is this repository for? ###

Code archaeology - it's the first thing I ever put on the App Store.

### Contribution guidelines ###

Read, be amused. Don't contribute because the ways of doing things here are probably very outdated. I keep meaning to rewrite this in Swift but haven't got around to it.