#import <UIKit/UIKit.h>
//#import <Foundation/Foundation.h>
#import "constants.h"


@interface candidateWordView : UIView {
	NSMutableArray*	currentWord;
}

-(id)initWithFrame:(CGRect)frame;

-(void)addChar:(char)newChar;
-(void)removeChar;
-(void)clearChars;
	
@property (nonatomic, retain) NSMutableArray* currentWord;

@end
