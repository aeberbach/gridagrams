//
//  GestureCatcher.m
//  TargetAnagrams
//
//  Created by Adam Eberbach on 9/02/10.
//  Copyright 2010 Adam Eberbach. All rights reserved.
//

#import "GestureCatcher.h"

@implementation GestureCatcher

@synthesize delegate;
@synthesize startTouchPosition;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
    UITouch *touch = [touches anyObject];
    // startTouchPosition is an instance variable
    startTouchPosition = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self];
	
    // To be a discard swipe, direction of touch must be horizontal, to the left, and long enough.
    if (fabsf(startTouchPosition.x - currentTouchPosition.x) >= HORIZ_SWIPE_DRAG_MIN &&
        fabsf(startTouchPosition.y - currentTouchPosition.y) <= VERT_SWIPE_DRAG_MAX)
    {
        // It appears to be a swipe but was it left?
        if (startTouchPosition.x > currentTouchPosition.x)
            [self.delegate gestureCaught:kGestureDiscard];
    }

	// To be an accept swipe, direction of touch must be vertical, down, and long enough.
    if (fabsf(startTouchPosition.y - currentTouchPosition.y) >= VERT_SWIPE_DRAG_MIN &&
        fabsf(startTouchPosition.x - currentTouchPosition.x) <= HORIZ_SWIPE_DRAG_MAX)
    {
        // It appears to be a swipe but was it down?
        if (startTouchPosition.y < currentTouchPosition.x)
            [self.delegate gestureCaught:kGestureAccept];
    }
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    startTouchPosition = CGPointZero;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    startTouchPosition = CGPointZero;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
		self.backgroundColor = [UIColor clearColor];
		self.userInteractionEnabled = YES;
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
}


- (void)dealloc {
    [super dealloc];
}


@end
