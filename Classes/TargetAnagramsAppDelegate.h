//
//  TargetAnagramsAppDelegate.h
//  TargetAnagrams
//
//  Created by Adam Eberbach on 20/09/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TargetAnagramsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	UIViewController *gridViewController;
}

@property (nonatomic, retain) IBOutlet UIViewController *gridViewController;
@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

