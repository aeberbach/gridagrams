//
//  GestureCatcher.h
//  TargetAnagrams
//
//  Created by Adam Eberbach on 9/02/10.
//  Copyright 2010 Adam Eberbach. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	kGestureDiscard,
	kGestureAccept
} gestureType;

@class GestureCatcher;

@protocol GestureCatcherDelegate <NSObject>

// report a gesture caught by this object
-(void)gestureCaught:(gestureType)type;

@end

@interface GestureCatcher : UIView {
	CGPoint startTouchPosition;
	id <GestureCatcherDelegate> delegate;
}

@property (nonatomic, assign) CGPoint startTouchPosition;
@property (nonatomic, retain) id <GestureCatcherDelegate> delegate;

// minimum distances to be recorded as a swipe
#define HORIZ_SWIPE_DRAG_MIN  (12)
#define VERT_SWIPE_DRAG_MAX   (4)

#define HORIZ_SWIPE_DRAG_MAX  VERT_SWIPE_DRAG_MAX
#define VERT_SWIPE_DRAG_MIN   HORIZ_SWIPE_DRAG_MIN

@end
