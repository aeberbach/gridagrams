/*
 *  constants.h
 *  TargetAnagrams
 *
 *  Created by Adam Eberbach on 3/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#define GRID_COUNT			(9) // number of squares in the grid and hence longest possible word
#define TARGET_CHAR_INDEX	(5)	// the index of the center of the grid
typedef enum {
	kWordAcceptable,
	kWordNotAcceptable,
	kWordAlreadyRecorded
} WordStatus;