//
//  TargetAnagramsAppDelegate.m
//  TargetAnagrams
//
//  Created by Adam Eberbach on 20/09/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import "TargetAnagramsAppDelegate.h"
#import "GridView.h"

@implementation TargetAnagramsAppDelegate

@synthesize window;
@synthesize gridViewController;

- (void)applicationDidFinishLaunching:(UIApplication *)application {    

	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {	
		gridViewController = [[GridView alloc] initWithNibName:@"GridView_iPad" bundle:nil];
	} else {
		gridViewController = [[GridView alloc] initWithNibName:@"GridView" bundle:nil];
	}
    // Override point for customization after application launch
	[window addSubview:gridViewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
	[gridViewController release];
    [window release];
    [super dealloc];
}


@end
