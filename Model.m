//
//  Model.m
//  FindAnagrams
//
//  Created by Adam Eberbach on 26/04/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
//	1. Finding a solution:
// if the word list has each word sorted without rearranging the order of words then if the mystery word is
// also sorted it can easily be checked against the word list to see if there is a match. If there is a match
// in the sorted array then the index of the unsorted (plaintext) word is the same as the word matched in the
// sorted array
//
// 2. Creating a new puzzle
// if the sorted wordlist is examined for 9-letter words and the indices of those words is recorded in an array
// when they are found then it is possible to select randomly from the indices to choose a new mystery word.
// 
// 3. Finding the number of words that can be created from this word and wordlist
// by taking the key character and then taking every possible combination of the remaining chars, sorting them
// and checking them against the sorted array every word in the list that can be made from the anagram is
// found. When just a count is needed, take the length of the array returned, when a solution is needed take
// indices from the array and apply to the unsorted array
// 

/*
 3 stages of readiness:
 1. wordsReady: the grid can be displayed and the grid entries can be checked against the unsortedWord list
	(ready to play)
 2. dictionaryReady: (internal use) the solution dictionary is ready, solution for this word can be found
 3. solutionReady: the solution for this word is ready and the UI can now display the word solution
 */
#import "Model.h"

@implementation Model

@synthesize solutionDictionary;
@synthesize foundWords;
@synthesize sortedWords;
@synthesize unsortedWords;
@synthesize nineLetterWords;
@synthesize currentGridWord;
@synthesize currentScrambledWord;
@synthesize keyValue;
@synthesize answerSet;

@synthesize solutionReady;
@synthesize wordsReady;
@synthesize dictionaryReady;

// char comparison: returns negative if b > a and positive if a > b
int char_cmp(const void *a, const void *b)
{
	const char *ia = (const char *)a;
    const char *ib = (const char *)b;
    return *ia  - *ib; 
}

/*************************************************************************************/
// Interface

// start a new puzzle - most of the work here is on the UI side
// return the scrambled word
- (NSMutableString*)newScrambledWord {
	
	// do nothing unless setSolutionReady == YES, indicating that the secondary task
	// is not currently processing the previous solution word. Don't allow two quick
	// taps on New to crash everything.
	
	if(solutionReady == YES) {
		
		// any existing solution is invalid
		self.solutionReady = NO;
	
		// clear all found words from the list
		[foundWords removeAllObjects];
	
		// choose a new scrambled 9-letter word
		currentGridWord = [self freshGridWord];
	
		currentScrambledWord = [self scrambleWord:currentGridWord];

		// start a new task to begin calculating all possible answers for this new word
		// both to handle "Solve" and to make checking words more efficient
		[NSThread detachNewThreadSelector:@selector(findAnswers) toTarget:self withObject:nil];
	
		// record the key value from the center of the scrambled word
		[keyValue release];
		char scrambledWord[GRID_COUNT + 1];
		[currentScrambledWord getCString:scrambledWord maxLength:(GRID_COUNT + 1) encoding:NSASCIIStringEncoding];
		keyValue = [[NSNumber alloc] initWithChar:(*(scrambledWord + TARGET_CHAR_INDEX - 1))];
	
		// return the current scrambled word
		return currentScrambledWord;
	}
	return nil;
}

- (void)findAnswers {
	
	// Secondary thread, create autorelease pool
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	[answerSet release];
	
	// don't proceed until the dictionary is built
	while(dictionaryReady == NO) {
		[NSThread sleepForTimeInterval:1.0];
	}
	answerSet = [[NSArray alloc] initWithArray:[self solveWords:currentScrambledWord]];
	
	// don't go find a solution until the dictionary is loaded or there are no answers to find
	[self setSolutionReady:YES];
	
	[pool release];
}

// add this word to the list of found words
- (void)wordSubmit:(NSMutableArray*)wordArray {
	[foundWords addObject:[self wordFromArray:wordArray]];
	
}


// is the current word acceptable, not acceptable or already found?
- (WordStatus)checkWord:(NSMutableArray*)wordArray {

	// first up - word must have the central key square
	if(![wordArray containsObject:keyValue])
		return kWordNotAcceptable;
	
	NSString* thisWord = [self wordFromArray:wordArray];

	// second - may have this word already
	if([foundWords containsObject:thisWord])
		return kWordAlreadyRecorded;

	// next see if it is in the dictionary
	// What is better?
	// 1. getting the Cstring, doing qsort, using dictionary to check for word?
	// 2. doing a containsObject on unsortedWords?
	if([unsortedWords containsObject:thisWord])
		return kWordAcceptable;
		
	// finally see if it is the 9-letter word
	if([currentGridWord compare:thisWord] == NSOrderedSame)
		return kWordAcceptable;
	// then give up
	return kWordNotAcceptable;
}

/*************************************************************************************/
// Private

- (NSMutableString*)wordFromArray:(NSMutableArray*)wordArray {

	// the array of characters is converted to a string
	NSMutableString* newWord = [NSMutableString stringWithCapacity:GRID_COUNT];
	/*
	char c;
	
	NSEnumerator* charEnumerator = [wordArray objectEnumerator];
	while(c = [[charEnumerator nextObject] charValue])
		[newWord appendFormat:@"%c", c];
	*/
	for(NSNumber* number in wordArray)
		[newWord appendFormat:@"%c", [number charValue]];
	return newWord;
}

// randomly choose a word from the list of 9-letter words
- (NSMutableString*)freshGridWord {
	
	int guessIndex;
	int wordlistLength = [nineLetterWords count];
	srandom(time(NULL));
	guessIndex = random() % wordlistLength + 1;
	return([nineLetterWords objectAtIndex:guessIndex]);
}

// scramble an input word for display in the grid
// http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
- (NSMutableString*)scrambleWord:(NSMutableString*)inputWord {
	srandom(time(NULL));
	int rndIndex;
	char swapChar;
	char shuffled[GRID_COUNT];
	
	strncpy(shuffled, [inputWord UTF8String], GRID_COUNT);
	
	int n = 8;
	do
	{
		rndIndex = random() % 8;
		swapChar = shuffled[n];
		shuffled[n--] = shuffled[rndIndex];
		shuffled[rndIndex] = swapChar;
	}while(n > 0);
	
	return [[NSMutableString alloc] initWithCString:shuffled length:GRID_COUNT];
}

/*************************************************************************************/
// init:
// nineLetterWords must be loaded first - new will require it to pick a new grid word
// new thread spawned to get setup happening while things appear onscreen
- (id)init {
	[super init];
		
	self.wordsReady = NO;
	self.dictionaryReady = NO;
	// to make the New/Solve state machine work this must begin as YES as if a puzzle was
	// in progress and solution processing for it had completed.
	solutionReady  = YES;

	// get things started quickly(ish) by loading just the 9-letter words
	NSString* fileContents = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sowpods9.txt" ofType:nil] encoding:NSUTF8StringEncoding error:nil]; 

	NSEnumerator *wordEnumerator = [[fileContents componentsSeparatedByString:@"\r\n" ] objectEnumerator];
	NSString *enumeratedWord;
	NSMutableArray* newWords = [[NSMutableArray array] retain];
	while(enumeratedWord = [wordEnumerator nextObject])
	{
		[newWords addObject:enumeratedWord];
	}
	self.nineLetterWords = (NSArray*)newWords;
	
	// detach a thread to load and manipulate the remaining word data
	[NSThread detachNewThreadSelector:@selector(loadRemainingWordData) toTarget:self withObject:nil];
	
	// stores the words found so far
	foundWords = [[NSMutableArray alloc] initWithCapacity:MAX_FIND_WORDS];
	// store the centre value (key character)
	keyValue = [[NSNumber alloc] init];
	
	return self;
}

- (void)dealloc {
	[solutionDictionary release];
	[nineLetterWords release];
	[foundWords release];
	[sortedWords release];
	[unsortedWords release];
	[super dealloc];

}
/* useful to check this
#import <mach/mach.h>
#import <mach/mach_host.h>

void print_free_memory() {
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);        
	
    vm_statistics_data_t vm_stat;
	
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
        NSLog(@"Failed to fetch vm statistics");
	}
    // Stats in bytes
    natural_t mem_used = (vm_stat.active_count +
                          vm_stat.inactive_count +
                          vm_stat.wire_count) * pagesize;
    natural_t mem_free = vm_stat.free_count * pagesize;
    natural_t mem_total = mem_used + mem_free;
    NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
}
 
*/

// Now sorted words corresponds 1:1 with unsorted words. What needs to happen for fast access when looking
// for solutions is for the arrays to be combined into a dictionary with sorted words as key and unsorted
// words as values. Because many words in the dictionary are anagrams of each other there will be multiple
// identical keys which is not allowed. So the dictionary has to look like this:
//
// NSDictionary (accesed by key) ---------- NSArray - NSString
//                                  |
//                                  ------- NSArray --- NSString
//                                                   |
//                                                   -- NSString
//
// In the first case the key gives a value that is a NSArray with one member, a string. This is the case when
// a word in sortedWords appears only once. The second case is an example of a sortedWord appearing twice, there
// are two unsorted words corresponding to that sorted key.
//
// Building:
//
// for all words in sortedWords
//	getObjectForKey (key is NSString from sortedWords)
//		if nil, create NSArray with NSString from unsortedWords at same index as current word from sortedWords
//		if not nil, add NSString from unsortedWords at same index as current word from sortedWords to array
//
// Accessing:
// 
// for each selectedString
//	getObjectForKey (key is selectedString created in solvedWords:)
//		if nil, no action
//		if valid, iterate over returned NSArray adding NSStrings to result NSArray
//
// writing this dictionary file to avoind building it every run seemed like a good idea but building it is quick
// and the resulting plist is nearly 7MB.

- (void)createSolutionDictionary {
	
	NSMutableDictionary* newDictionary = [NSMutableDictionary dictionary];
	
	// count manually rather than getting the index from sortedWord
	int sortedIndex = 0;
	
	// for each word in sortedWords
	for(NSString* sortedWord in sortedWords) {
		
		// get the corresponding unsortedWord
		NSString* unsortedWord = [unsortedWords objectAtIndex:sortedIndex++];
		
		// get the array stored as value in the dictionary where this word is the key
		NSMutableArray* dictionaryValuesArray = [newDictionary objectForKey:sortedWord];
		
		if(dictionaryValuesArray == nil) {
			// this word isn't a key yet. Create a new array using the unsortedWord string
			dictionaryValuesArray = [NSMutableArray arrayWithObject:unsortedWord];
			// add it to the dictionary using the sortedWord as key
			[newDictionary setObject:dictionaryValuesArray forKey:sortedWord];
		} else {
			// word is a key already, unsortedWord is added to the existing array
			[dictionaryValuesArray addObject:unsortedWord];
		}
	}
	self.solutionDictionary = newDictionary;
	
}

// load the rest of the word list stripped of words greater than 9 letters for obvious reasons - cuts size of data by well over half
- (void)loadSmallerWords {
	
	NSString* fileContents = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"smallwords.txt" ofType:nil] encoding:NSUTF8StringEncoding error:nil]; 
	NSEnumerator* wordEnumerator = [[fileContents componentsSeparatedByString:@"\r\n" ] objectEnumerator];
	NSMutableArray* newWords = [NSMutableArray array];
	NSString* enumeratedWord;
	while(enumeratedWord = [wordEnumerator nextObject])
	{
		[newWords addObject:enumeratedWord];
	}
	self.unsortedWords = (NSArray*)newWords;
}

// sort all of the words in unsortedWords into the new array sortedWords
- (void)sortSmallerWords {
	
	NSMutableArray* newWords = [NSMutableArray array];
	
	char temp[GRID_COUNT+1];
	for(NSString* unsortedWord in unsortedWords)
	{
		[unsortedWord getCString:temp maxLength:GRID_COUNT+1 encoding:NSASCIIStringEncoding];
		qsort(temp, strlen(temp), sizeof(char), char_cmp);
		[newWords addObject:[NSString stringWithCString:temp encoding:NSASCIIStringEncoding]];
	}
	self.sortedWords = (NSArray*)newWords;
}

/*************************************************************************************/
// Secondary thread at startup: get the word data loaded without hanging up the UI
- (void)loadRemainingWordData {
	
    // Secondary thread, create autorelease pool
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	// load the remaining words (the ones smaller than nine letters) into unsortedWords
	[self loadSmallerWords];

	// cause the main thread to set wordsReady (words can now be checked)
	[self performSelectorOnMainThread:@selector(wordsLoaded) withObject:nil waitUntilDone:NO];

	// create the array of sortedWords used for checking answers
	[self sortSmallerWords];
	
	// create the dictionary of solutions for this set of words (solution can now be built)
	[self createSolutionDictionary];
	
	// cause the main thread to set dictionaryReady
	[self performSelectorOnMainThread:@selector(dictionaryLoaded) withObject:nil waitUntilDone:NO];
	
    [pool release];
}

- (void)wordsLoaded {
	[self setWordsReady:YES];
}

- (void)dictionaryLoaded {
	[self setDictionaryReady:YES];
}

- (NSArray*)solve {
	// return the set of answers in alphabetical order
	return [answerSet sortedArrayUsingSelector:@selector(compare:)];
}

/*
 It would be good to sort the permuted string only once but the key character must be present
 in every string. So the 9 characters are sorted, then the position of the key noted and the
 key is removed. If the position of the key is known then the number of places before it in the
 larger string is known. So the simple process of selecting characters based on the bit pattern
 of every value 0..255 is valid as long as the position of the key character relative to the
 selected characters is preserved. So:
 
 AFFLUENCE gets scrambled to NEFLFACUE with key character F. Sorted it becomes ACEEFFLNU.
 The key is in index 4 (0-based). 
 As example, current bit pattern is 11101001. 
 - add char A at index 0
 - add char C at index 1
 - add char E at index 2
 - skip char E at index 3
 - add key char F at index 4
 - add char F at index 5
 - skip char L at index 6
 - skip char N at index 7
 - add char U at index 8
 Word generated for checking against sortedWords is ACEFFU - sorted! One qsort per word check.
 
 The first approach of removing the key then selecting characters according to bit pattern,
 finally adding the key again and sorting before checking for matches in sortedWords did about 254
 unneccessary qsorts. But that wasn't the issue, it was using indexOfObject on an array with 
 > 100,000 objects. Which is why you find out the things that matter before optimizing.
 */

// given the mystery word, find all matches in the dictionary. Assume key is at index KEY_CHARACTER
- (NSArray*)solveWords:(NSString*)inputWord {

	// nine characters, one is fixed, 256 combinations to be selected
	NSMutableArray* matchingWords = [NSMutableArray array];
	NSMutableArray* selectedStrings = [NSMutableArray array];
	// the destination for the selected string
	NSString* selectedString;
	// a bit pattern of all 1's, decremented until it is all 0's
	unsigned char eightBits = 0xff;
	unsigned char rotateBits;
	unsigned char mask = 0x80;
	// working string for building the selected word in
	char testString[GRID_COUNT + 1];
	// holds the Cstring representation of the input word
	char wordChars[GRID_COUNT + 1];
	// the key character (centre of the grid)
	char keyChar = [inputWord characterAtIndex:KEY_CHARACTER];
	int foundIndex;
	// get the characters from the input word
	[inputWord getCString:wordChars maxLength:(GRID_COUNT + 1) encoding:NSASCIIStringEncoding];
	
	// sort it
	qsort(wordChars, strlen(wordChars), sizeof(char), char_cmp);

	// record the position of the key character
	int keyPos = 0;
	while(*(wordChars + keyPos) != keyChar)
		keyPos++;
	
	// generate 256 possible words to test of 8 chars + one fixed char
	while(eightBits) {
		
		// generate the word for this bit pattern (copying the pattern to preserve the counter)
		rotateBits = eightBits;
		// position of next character in testString
		int currentPosition = 0;
		
		for(foundIndex = 0; foundIndex < GRID_COUNT; foundIndex++) {
			
			// key position - add the key character and skip over it
			if(foundIndex == keyPos) {
				*(testString + currentPosition++) = keyChar;
				continue;
			}
				
			// not the key position, check for character inclusion based on bit pattern
			if(mask & rotateBits)
				*(testString + currentPosition++) = wordChars[foundIndex];
			
			// left-shift the pattern to position the relevant bit to check for inclusion of the next character
			rotateBits <<= 1;
		}
		// null terminate generated string
		*(testString + currentPosition) = '\0';

		// if the scrambled word contains multiple of a char it is possible to have multiple copies of the
		// same testString generated. Not adding them to the matchingWords array is quicker than turning
		// matchingWords into a NSSet and then back again to ensure uniqueness.
		selectedString = [NSString stringWithCString:testString encoding:NSASCIIStringEncoding];
		if(![selectedStrings containsObject:selectedString]) {
			[selectedStrings addObject:selectedString];
		}
		// next bit pattern
		eightBits--;
	}
	
	for(selectedString in selectedStrings) {
		
		NSArray* currentDictionaryArray = [solutionDictionary objectForKey:selectedString];
		for(NSString* matchingString in currentDictionaryArray)
			[matchingWords addObject:matchingString];
	}
	// and add the nine-letter word
	[matchingWords addObject:currentGridWord];
	
	return(matchingWords);
}

@end
