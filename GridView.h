//
//  GridView.h
//  TargetAnagrams
//
//  Created by Adam Eberbach on 20/09/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
#import "candidateWordView.h"
#import "foundWordsView.h"

#import "constants.h"

@interface GridView : UIViewController {
	
	// activity indicator
	IBOutlet UIActivityIndicatorView* activityIndicator;
	
	// the 9 buttons in the 3x3 grid
    IBOutlet UIButton *gridButton1;
    IBOutlet UIButton *gridButton2;
    IBOutlet UIButton *gridButton3;
    IBOutlet UIButton *gridButton4;
    IBOutlet UIButton *gridButton5;
    IBOutlet UIButton *gridButton6;
    IBOutlet UIButton *gridButton7;
    IBOutlet UIButton *gridButton8;
    IBOutlet UIButton *gridButton9;
	
	// new, solve, accept and backspace buttons
    IBOutlet UIButton *newButton;
    IBOutlet UIButton *solveButton;
    IBOutlet UIButton *acceptButton;
    IBOutlet UIButton *bsButton;
	
	// the grid itself
    IBOutlet UIImageView *gridImageView;
	
	// the newsprint background
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIProgressView *findProgress;
    IBOutlet UITextView *foundWordsTextView;
	
    IBOutlet UILabel *progressLabel;
    IBOutlet UILabel *wordLabel;
	
	// array storing the scrambled grid word
	NSString* gridWord;
	// array storing word being constructed
	NSMutableArray* candidateWord;
	NSMutableString* candidateWordString;
	
	// array storing the current buttons in order they are pressed
	NSMutableArray *gridButtonPresses;

	NSMutableString *foundWordsList;
	// array storing 
	// game model implementing anagrams
	Model*	gameModel;
	
	int foundWordsCount;
	int solutionCount;
}
@property int foundWordsCount;
@property int solutionCount;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView* activityIndicator;

@property (nonatomic, retain) IBOutlet UIProgressView *findProgress;
@property (nonatomic, retain) IBOutlet NSMutableString *foundWordsList;
@property (nonatomic, retain) IBOutlet UITextView *foundWordsTextView;
@property (nonatomic, retain) IBOutlet UILabel *progressLabel;
@property (nonatomic, retain) IBOutlet UILabel *wordLabel;

@property (nonatomic, retain) IBOutlet UIButton *gridButton1;
@property (nonatomic, retain) IBOutlet UIButton *gridButton2;
@property (nonatomic, retain) IBOutlet UIButton *gridButton3;
@property (nonatomic, retain) IBOutlet UIButton *gridButton4;
@property (nonatomic, retain) IBOutlet UIButton *gridButton5;
@property (nonatomic, retain) IBOutlet UIButton *gridButton6;
@property (nonatomic, retain) IBOutlet UIButton *gridButton7;
@property (nonatomic, retain) IBOutlet UIButton *gridButton8;
@property (nonatomic, retain) IBOutlet UIButton *gridButton9;

@property (nonatomic, retain) IBOutlet UIButton *newButton;
@property (nonatomic, retain) IBOutlet UIButton *solveButton;
@property (nonatomic, retain) IBOutlet UIButton *acceptButton;
@property (nonatomic, retain) IBOutlet UIButton *bsButton;

@property (nonatomic, retain) IBOutlet UIImageView *gridImageView;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, retain) NSString* gridWord;
@property (nonatomic, retain) NSMutableArray* candidateWord;
@property (nonatomic, retain) NSMutableString* candidateWordString;
@property (nonatomic, retain) NSMutableArray* gridButtonPresses;
@property (nonatomic, retain) Model* gameModel;

- (void)updateFoundWords:(NSString*)newWord;
- (void)setAcceptButtonMode:(WordStatus)status;
- (void)setGridButtonMode:(int)tag enabled:(Boolean)active;
- (UIImage*)imageForCharacter:(char)thisChar;
- (void)wordEntryStartState;
- (IBAction)acceptWordButton:(id)sender;
- (IBAction)gridButtons:(id)sender;
- (IBAction)newButton:(id)sender;
- (IBAction)solveButton:(id)sender;
- (IBAction)bsButton:(id)sender;
- (void)progressComment;

@end
